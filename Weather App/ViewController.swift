//
//  ViewController.swift
//  Weather App
//
//  Created by Click Labs on 1/21/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    // textfield variable...
    @IBOutlet weak var enterYourCity: UITextField!
    
    // labels...
    @IBOutlet weak var displayWeatherLabel: UILabel!
    @IBOutlet weak var maxTemperatureNotic: UILabel!
    @IBOutlet weak var maxTemperatureLabel: UILabel!
    @IBOutlet weak var minTempretureNotice: UILabel!
    @IBOutlet weak var minTemperatureLabel: UILabel!
    
    
    @IBAction func findWeatherButton(sender: AnyObject) {
        
        //set url from where Weather message will retrieve...
        var urlOfWeather = "http://www.weather-forecast.com/locations/" + enterYourCity.text.stringByReplacingOccurrencesOfString(" ", withString: "") + "/forecasts/latest"
        
        var url = NSURL(string: urlOfWeather)
        
        //access web content...
        let task = NSURLSession.sharedSession().dataTaskWithURL(url!)
        { (data, reponse, erroe) in
            
            var urlContent = NSString(data: data, encoding: NSUTF8StringEncoding)
        
            //"<span class=\"phrase\">" breaks the whole message into two part...first part is 0...and second part is 1...
            var contentArray = NSString(string: urlContent!).componentsSeparatedByString("<span class=\"phrase\">") as [String]
            
            //set condition to prevent to crash the code on wrong input...
            if NSString(string: urlContent!).containsString("<span class=\"phrase\">")
            {
                    //take part 1 and again "</span>"  breaks the whole message into two part...first part is 0...and second part is 1...
                    var newContentArray = contentArray[1].componentsSeparatedByString("</span>")
                
                    //take part 0 before "</span>" and replace &deg to º(degree)...
                    var weatherForecast = newContentArray[0].stringByReplacingOccurrencesOfString("&deg", withString: "º" ) as NSString
                
                    //find maximum temperature...
                     var maxTemperatureArray = weatherForecast.componentsSeparatedByString("max")
                     var maximumTemperature = maxTemperatureArray[1].componentsSeparatedByString(";")
                
                     //find minimum temperature...
                     var minTemperatureArray = weatherForecast .componentsSeparatedByString("min")
                     var minimumTemperature = minTemperatureArray[1].componentsSeparatedByString(";")
                
                
                    //chek wheathe city is found or not, if found then...
                    dispatch_async(dispatch_get_main_queue())
                    {
                        println(weatherForecast)
                        self.displayWeatherLabel.text = weatherForecast
                      
                        //display the maximum temprature...
                        self.maxTemperatureNotic.hidden = false
                        self.maxTemperatureLabel.hidden = false
                        var maxTemperatureInCelsius = maximumTemperature[0] as? String
                        self.maxTemperatureLabel.text = maxTemperatureInCelsius! + "C"
                       
                        //display the minimum temprature...
                        self.minTempretureNotice.hidden = false
                        self.minTemperatureLabel.hidden = false
                        var minTemperatureInCelsius = minimumTemperature[0] as? String
                        self.minTemperatureLabel.text = minTemperatureInCelsius! + "C"
                    }
            }//end of if condition...
            else
            {
                    dispatch_async(dispatch_get_main_queue())
                    {
                        self.maxTemperatureNotic.hidden = true
                        self.maxTemperatureLabel.hidden = true
                        
                        self.minTempretureNotice.hidden = true
                        self.minTemperatureLabel.hidden = true
                        
                        self.displayWeatherLabel.text = "Could Not Find your City, Try again..."
                    }
            }//end of else condition...

        }//end of NSURLSession.sharedSession() condition...
        
        task.resume()
        self.view.endEditing(true)
    }//end of button function...
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        self.view.endEditing(true)
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        enterYourCity.resignFirstResponder()
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

